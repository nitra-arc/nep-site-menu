<?php

namespace Nitra\MenuBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document(collection="Menus")
 */
class Menu
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var boolean Is active menu
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var \Nitra\StoreBundle\Document\Store Store reference
     * @ODM\ReferenceOne(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $store;

    /**
     * @var Item[] Menu items
     * @ODM\EmbedMany(targetDocument="Item")
     */
    protected $items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function setStore(\Nitra\StoreBundle\Document\Store $store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     * @return \Nitra\StoreBundle\Document\Store $store
     */
    public function getStore()
    {
        return $this->store;
    }
    
    /**
     * Add menuItem
     * @param \Nitra\MenuBundle\Document\Item $item
     * @return self
     */
    public function addItem(\Nitra\MenuBundle\Document\Item $item)
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * Remove menuItem
     * @param \Nitra\MenuBundle\Document\Item $item
     * @return self
     */
    public function removeItem(\Nitra\MenuBundle\Document\Item $item)
    {
        $this->items->removeElement($item);
        return $this;
    }

    /**
     * Get menuItem
     * @return \Nitra\MenuBundle\Document\Item[] $items
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set menuItem
     * @param \Nitra\MenuBundle\Document\Item[] $items
     * @return self
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }
}