<?php

namespace Nitra\MenuBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\EmbeddedDocument
 */
class Item
{
    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Title
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @var string Description
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var boolean Is active
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var string Item type
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    protected $type;

    /**
     * @var string Icon
     * @ODM\String
     */
    protected $image;

    /**
     * @var string Css class
     * @ODM\String
     * @Assert\Length(max="255")
     */
    protected $class;

    /**
     * @var boolean Show title flag
     * @ODM\Boolean
     */
    protected $showTitle;

    /**
     * @var boolean Is group flag
     * @ODM\Boolean
     */
    protected $isGroup;

    /**
     * @var integer Amount of sub columns
     * @ODM\Int
     * @Assert\Length(max="11")
     */
    protected $columns;

    /**
     * @var string Width of each column
     * @ODM\String
     */
    protected $detailColumnsWidth;
    
    /**
     * @var string Type of item sub menu
     * @ODM\String
     * @Assert\Length(max="255")
     */
    protected $submenuType;

    /**
     * @var string Html data of sub menu content
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $submenuContent_html;

    /**
     * @var string Id of parent item
     * @ODM\String
     */
    protected $parent;

    /**
     * @var string Url content
     * @ODM\String
     */
    protected $typeContent_url;

    /**
     * @var \Nitra\ProductBundle\Document\Category Category reference
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $typeContent_category;

    /**
     * @var \Nitra\InformationBundle\Document\InformationCategory Category of articles reference
     * @ODM\ReferenceOne(targetDocument="Nitra\InformationBundle\Document\InformationCategory")
     */
    protected $typeContent_infoCategory;

    /**
     * @var \Nitra\InformationBundle\Document\Information Article reference
     * @ODM\ReferenceOne(targetDocument="Nitra\InformationBundle\Document\Information")
     */
    protected $typeContent_information;

    /**
     * @var \Nitra\ProductBundle\Document\Product Product reference
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    protected $typeContent_product;

    /**
     * @var \Nitra\ProductBundle\Document\Badge Badge reference
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Badge")
     */
    protected $typeContent_badge;

    /**
     * @var string Html content
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $typeContent_html;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set type
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set class
     * @param string $class
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get class
     * @return string $class
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set showTitle
     * @param boolean $showTitle
     * @return self
     */
    public function setShowTitle($showTitle)
    {
        $this->showTitle = $showTitle;
        return $this;
    }

    /**
     * Get showTitle
     * @return boolean $showTitle
     */
    public function getShowTitle()
    {
        return $this->showTitle;
    }

    /**
     * Set isGroup
     * @param boolean $isGroup
     * @return self
     */
    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;
        return $this;
    }

    /**
     * Get isGroup
     * @return boolean $isGroup
     */
    public function getIsGroup()
    {
        return $this->isGroup;
    }

    /**
     * Set columns
     * @param int $columns
     * @return self
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * Get columns
     * @return int $columns
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Set detailColumnsWidth
     * @param string $detailColumnsWidth
     * @return self
     */
    public function setDetailColumnsWidth($detailColumnsWidth)
    {
        $this->detailColumnsWidth = $detailColumnsWidth;
        return $this;
    }

    /**
     * Get detailColumnsWidth
     * @return string $detailColumnsWidth
     */
    public function getDetailColumnsWidth()
    {
        return $this->detailColumnsWidth;
    }

    /**
     * Set submenuType
     * @param string $submenuType
     * @return self
     */
    public function setSubmenuType($submenuType)
    {
        $this->submenuType = $submenuType;
        return $this;
    }

    /**
     * Get submenuType
     * @return string $submenuType
     */
    public function getSubmenuType()
    {
        return $this->submenuType;
    }

    /**
     * Set parent
     * @param string $parent
     * @return self
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     * @return string $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set typeContentCategory
     * @param \Nitra\ProductBundle\Document\Category $typeContentCategory
     * @return self
     */
    public function setTypeContentCategory(\Nitra\ProductBundle\Document\Category $typeContentCategory)
    {
        $this->typeContent_category = $typeContentCategory;
        return $this;
    }

    /**
     * Get typeContentCategory
     * @return \Nitra\ProductBundle\Document\Category $typeContentCategory
     */
    public function getTypeContentCategory()
    {
        return $this->typeContent_category;
    }

    /**
     * Set typeContentUrl
     * @param string $typeContentUrl
     * @return self
     */
    public function setTypeContentUrl($typeContentUrl)
    {
        $this->typeContent_url = $typeContentUrl;
        return $this;
    }

    /**
     * Get typeContentUrl
     * @return string $typeContentUrl
     */
    public function getTypeContentUrl()
    {
        return $this->typeContent_url;
    }

    /**
     * Set typeContentInformationCategory
     * @param \Nitra\InformationBundle\Document\InformationCategory $typeContentInformationCategory
     * @return self
     */
    public function setTypeContentInfoCategory($typeContentInformationCategory)
    {
        $this->typeContent_infoCategory = $typeContentInformationCategory;
        return $this;
    }

    /**
     * Get typeContentInformationCategory
     * @return \Nitra\InformationBundle\Document\InformationCategory $typeContentInformationCategory
     */
    public function getTypeContentInfoCategory()
    {
        return $this->typeContent_infoCategory;
    }

    /**
     * Set typeContentInformation
     * @param \Nitra\InformationBundle\Document\Information $typeContentInformation
     * @return self
     */
    public function setTypeContentInformation(\Nitra\InformationBundle\Document\Information $typeContentInformation)
    {
        $this->typeContent_information = $typeContentInformation;
        return $this;
    }

    /**
     * Get typeContentInformation
     * @return \Nitra\InformationBundle\Document\Information $typeContentInformation
     */
    public function getTypeContentInformation()
    {
        return $this->typeContent_information;
    }

    /**
     * Set typeContentProduct
     * @param \Nitra\ProductBundle\Document\Product $typeContentProduct
     * @return self
     */
    public function setTypeContentProduct(\Nitra\ProductBundle\Document\Product $typeContentProduct)
    {
        $this->typeContent_product = $typeContentProduct;
        return $this;
    }

    /**
     * Get typeContentProduct
     * @return \Nitra\ProductBundle\Document\Product $typeContentProduct
     */
    public function getTypeContentProduct()
    {
        return $this->typeContent_product;
    }

    /**
     * Set typeContentHtml
     * @param string $typeContentHtml
     * @return self
     */
    public function setTypeContentHtml($typeContentHtml)
    {
        $this->typeContent_html = $typeContentHtml;
        return $this;
    }

    /**
     * Get typeContentHtml
     * @return string $typeContentHtml
     */
    public function getTypeContentHtml()
    {
        return $this->typeContent_html;
    }

    /**
     * Set submenuContentHtml
     * @param string $submenuContentHtml
     * @return self
     */
    public function setSubmenuContentHtml($submenuContentHtml)
    {
        $this->submenuContent_html = $submenuContentHtml;
        return $this;
    }

    /**
     * Get submenuContentHtml
     * @return string $submenuContentHtml
     */
    public function getSubmenuContentHtml()
    {
        return $this->submenuContent_html;
    }

    /**
     * Set typeContentBadge
     * @param \Nitra\ProductBundle\Document\Badge $typeContentBadge
     * @return self
     */
    public function setTypeContentBadge(\Nitra\ProductBundle\Document\Badge $typeContentBadge)
    {
        $this->typeContent_badge = $typeContentBadge;
        return $this;
    }

    /**
     * Get typeContentBadge
     * @return \Nitra\ProductBundle\Document\Badge $typeContentBadge
     */
    public function getTypeContentBadge()
    {
        return $this->typeContent_badge;
    }
}