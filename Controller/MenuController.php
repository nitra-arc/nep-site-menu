<?php

namespace Nitra\MenuBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MenuController extends NitraController
{
    /**
     * @var \Nitra\MenuBundle\Document\Item[]
     */
    protected $children;

    /**
     * @var string
     */
    protected $imageFilter;

    /**
     * @var string
     */
    protected $pageUrl;

    /**
     * Render menu
     *
     * @Template("NitraMenuBundle::menu.html.twig")
     *
     * @param null|string $imageFilter
     * @param null|string $pageUrl
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function renderAction($imageFilter = null, $pageUrl = null)
    {
        $this->pageUrl = $pageUrl;

        if (!$imageFilter) {
            $imageFilter = 'category_menu';
        }

        $this->imageFilter = $imageFilter;

        $store = $this->getStore();

        $menu = $this->getDocumentManager()->createQueryBuilder('NitraMenuBundle:Menu')
            ->field('isActive')->equals(true)
            ->field('store.id')->equals($store['id'])
            ->getQuery()->getSingleResult();

        if (!$menu) {
            throw $this->createNotFoundException('Active menu for current store not found');
        }

        foreach ($menu->getItems() as $child) {
            if ($child->getIsActive()) {
                $this->children[$child->getParent()][] = $child;
            }
        }

        $items = $this->getSubItems('null');

        return array(
            'items' => $items,
        );
    }

    /**
     * Get sub items
     *
     * @param string $parentId
     *
     * @return array
     */
    protected function getSubItems($parentId)
    {
        if (!$this->hasChild($parentId)) {
            return array();
        }
        $subItems = array();
        foreach ($this->getNodes($parentId) as $item) {
            $subItems[$item->getId()] = $this->formatSubItem($item);
        }

        return $subItems;
    }

    /**
     * Format sub item menu
     *
     * @param \Nitra\MenuBundle\Document\Item $item
     *
     * @return array
     */
    protected function formatSubItem($item)
    {
        $subItem = array(
            'title'         => $item->getTitle(),
            'show_title'    => $item->getShowTitle(),
            'class'         => $item->getClass(),
            'link'          => $this->getLink($item),
            'is_group'      => $item->getIsGroup(),
            'columns'       => array(),//$this->getColumnWidth($item),
            'current'       => false,
        );

        if ($this->pageUrl != null && $subItem['link'] == $this->pageUrl) {
            $subItem['class'] = $subItem['class'] . ' active current';
            $subItem['current'] = true;
        }
        if ($item->getImage()) {
            $subItem['image'] = $this->applyImagineFilter($item->getImage());
        }
        if ($item->getDescription()) {
            $subItem['description'] = $item->getDescription();
        }
        if ($this->hasChild($item->getId()) && ($item->getSubmenuType() == 'menu')) {
            $subSubItems = $this->getSubItems($item->getId());
            if ($item->getColumns() > 1) {
                $i = 0;
                foreach ($subSubItems as $subSubItem) {
                    $idx = ($i % $item->getColumns()) + 1;
                    $tmpItems = array_key_exists($idx, $subItem['columns']) ? $subItem['columns'][$idx] : array();
                    $tmpItems[] = $subSubItem;
                    $subItem['columns'][$idx] = $tmpItems;
                    $i ++;
                }
            } else {
                foreach ($subSubItems as $sb) {
                    if ($sb['current']) {
                        $subItem['class'] = $subItem['class'] . ' active';
                    }
                }

                $subItem['columns'] = array(
                    1 => $subSubItems,
                );
            }
        } elseif ($item->getSubmenuType() == 'html') {
            $subItem['content'] = $item->getSubmenuContentHtml();
        }

        return $subItem;
    }

    /**
     * Check that child exists
     *
     * @param string $id
     *
     * @return bool
     */
    protected function hasChild($id)
    {
        return isset($this->children[$id]);
    }

    /**
     * Get nodes of children and parents
     *
     * @param string $id
     *
     * @return \Nitra\MenuBundle\Document\Item[]
     */
    protected function getNodes($id)
    {
        return $this->children[$id];
    }

    /**
     * Get string to put directly in the "src" of the tag <img>
     *
     * @param string $image
     *
     * @return string
     */
    protected function applyImagineFilter($image)
    {
        return $this->getLiipImagineCacheManager()
            ->getBrowserPath($image, $this->imageFilter);
    }

    /**
     * Create links for menu
     * @param \Nitra\MenuBundle\Document\Item $item
     * @return null|string
     */
    protected function getLink($item)
    {
        $routeName   = null;
        $routeParams = array();
        switch ($item->getType()) {
            case 'category':
                $routeName = 'category_product_page';
                $routeParams['slug'] = $item->getTypeContentCategory()->getAlias();
                break;
            case 'product':
                $routeName = 'product_page';
                $routeParams['slug'] = $item->getTypeContentProduct()->getAlias();
                break;
            case 'information':
                $routeName = 'information_page';
                $routeParams['slug'] = $item->getTypeContentInformation()->getAlias();
                break;
            case 'infoCategory':
                $routeName = 'information_category_page';
                $routeParams['slug'] = $item->getTypeContentInfoCategory()->getAlias();
                break;
            case 'badge':
                $routeName = 'badge_products_page';
                $routeParams['badgeIdentifier'] = $item->getTypeContentBadge()->getIdentifier();
                break;
        }
        if ($routeName) {
            return $this->get('router')->generate($routeName, $routeParams, false);
        } else {
            return preg_replace('/\{locale\}/', $this->getRequest()->getLocale(), $item->getTypeContentUrl());
        }
    }

    /**
     * Get width of columns
     *
     * @param \Nitra\MenuBundle\Document\Item $item
     *
     * @return array
     */
    protected function getColumnWidth($item)
    {
        $output = array();

        $detailColumnWidth = $item->getDetailColumnsWidth();
        $split = preg_split('#\s+#', $detailColumnWidth);
        if (!empty($split) && !empty($detailColumnWidth)) {
            foreach ($split as $sp) {
                $tmp = explode("=", $sp);
                if (count($tmp) > 1) {
                    $output[trim(preg_replace("#col#", "", $tmp[0]))] = (int) $tmp[1];
                }
            }
        }
        $tmp = array_sum($output);
        $spans = array();
        $t = 0;
        for ($i = 1; $i <= $item->getColumns(); $i++) {
            if (array_key_exists($i, $output)) {
                $spans[$i] = $output[$i];
            } else {
                if ((12 - $tmp) % ($item->getColumns() - count($output)) == 0) {
                    $spans[$i] = ((12 - $tmp) / ($item->getColumns() - count($output)));
                } else {
                    if ($t == 0) {
                        $spans[$i] = ( ((11 - $tmp) / ($item->getColumns() - count($output))) + 1 );
                    } else {
                        $spans[$i] = ( ((11 - $tmp) / ($item->getColumns() - count($output))) + 0 );
                    }
                    $t++;
                }
            }
        }

        return $spans;
    }
}